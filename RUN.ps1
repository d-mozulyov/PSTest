
$Env:CI_PIPELINE_ID = 12345678
$Local:CI_PIPELINE_ID = $Env:CI_PIPELINE_ID 


Write-Host "PowerShell $($Host.Version.ToString())"
Write-Host "Emulated `$CI_PIPELINE_ID = $CI_PIPELINE_ID"
Write-Host "Emulated `$Local:CI_PIPELINE_ID = $Local:CI_PIPELINE_ID"
Write-Host "Emulated `$Script:CI_PIPELINE_ID = $Script:CI_PIPELINE_ID"
Write-Host "Emulated `$Global:CI_PIPELINE_ID = $Global:CI_PIPELINE_ID"
Write-Host "Emulated `$Env:CI_PIPELINE_ID = $Env:CI_PIPELINE_ID"

& "$PSScriptRoot/myjob.ps1"

Write-Host "Press Enter to quit" -NoNewLine
Read-Host